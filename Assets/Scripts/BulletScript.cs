﻿using UnityEngine;
using System.Collections;

public class BulletScript : MonoBehaviour
{
		
	// 1 - Designer variables

	/// <summary>
	/// Damage inflicted
	/// </summary>
	public int damage = 1;

	/// <summary>
	/// Projectile damage player or enemies?
	/// </summary>
	public bool isEnemyShot = false;

	void Start ()
	{
		// 2 - Limited time to live to avoid any leak
		Destroy (gameObject, 10); // 20sec
	}

	void Update ()
	{
		
		transform.Translate (new Vector3 (0.1f, 0f, 0f));
	}


}
